module.exports = function(grunt) {
    grunt.initConfig({
        concat: {
            js: {
                src: ['src/js/*.js'],
                dest: 'build/js/scripts.js'
            },
            css: {
                src: ['src/css/*.css'],
                dest: 'build/css/styles.css'
            }
        },
        watch: {
            js: {
                files: ['js/**/*.js'],
                tasks: ['concat']
            },
            css: {
                files: ['css/**/*.css'],
                tasks: ['concat']
            }
        },
        'http-server': {
            dev: {
                root: 'dev/',
                port: 8080,
                host: '0.0.0.0',
                cache: -1,
                showDir: true,
                autoIndex: true,
                ext: "html",
                runInBackground: false,
                logFn: function(req, res, error) {
                    if (error)
                        console.log(error);
                }
            }
        },
    })
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-http-server');
    grunt.registerTask('default', ['concat','watch']);
    grunt.registerTask('server', ['http-server']);

};
