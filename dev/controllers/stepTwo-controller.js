angular.module('vagajs').controller('StepTwoController', function($scope, $location, plans, dataSteps, $routeParams) {
	// Valor que irá aparecer selecionado o <select>
	$scope.placeholder = 'Escolha um Plano';
    // Opções do select
    $scope.options = [];
    // Boolean que define se o formalário da etapa 3 será exibido
    $scope.formFinal = false;
    
    plans.get({sku: $routeParams.sku}, function(options) {
        $scope.options = options.planos;
    }, function(erro) {
        console.log(erro);
    });
            
    $scope.nextStep = function(){
        dataSteps.setPlan(this.selectedOption);
        $location.path( "/stepThree" );
    }
});