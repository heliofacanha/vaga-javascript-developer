angular.module('vagajs').controller('StepOneController', function($scope, $location, platforms, dataSteps) {
    // Valor que irá aparecer selecionado o <select>
    $scope.placeholder = 'Escolha uma Plataforma';
    // Opções do select
    $scope.options = [];
    // Boolean que define se o formalário da etapa 3 será exibido
    $scope.formFinal = false;
    
    platforms.get({}, function(options) {
        $scope.options = options.plataformas;
    }, function(erro) {
        console.log(erro);
    });
            
    $scope.nextStep = function(){
        dataSteps.setPlatform(this.selectedOption);
        $location.path( "/stepTwo/"+this.selectedOption);
    }
});