angular.module('vagajs').controller('StepThreeController', function($scope, $location, $routeParams, dataSteps) {
    // Boolean que define se o formalário da etapa 3 será exibido
    $scope.formFinal = true;

    $scope.onChange = function(value) {
        console.log(value);
    }
            
    $scope.submit = function(){
        dataSteps.setUser(this.user);
        var styleTitle = 'color: #163354; font-size: 24px;';
        var styleResponse = 'color: #A9C8E9; font-size: 24px;';
        
        console.clear();

        console.log('%cPlataforma: %c'+dataSteps.getData().platform, styleTitle, styleResponse);   
        console.log('%cPlano: %c'+dataSteps.getData().plan, styleTitle, styleResponse);
        console.log('%cDados Pessoais', styleTitle); 
        console.table(dataSteps.getData().user);
    }
});