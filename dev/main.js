angular.module('vagajs', ['directives','ngRoute', 'ngResource','services'])
	.config(function($routeProvider, $locationProvider) {

		$locationProvider.html5Mode(true);
		//Se a url for /stepOne, acessar primeira página
		$routeProvider.when('/stepOne', {
            templateUrl: 'views/steps.html',
			controller: 'StepOneController'
        });
		//Se a url for /stepOne, acessar segunda página        
		$routeProvider.when('/stepTwo/:sku', {
            templateUrl: 'views/steps.html',
			controller: 'StepTwoController'
        });
		//Se a url for /stepOne, acessar terceira e última página        
		$routeProvider.when('/stepThree', {
            templateUrl: 'views/steps.html',
			controller: 'StepThreeController'
		});

		$routeProvider.otherwise({redirectTo: '/stepOne'});
	});