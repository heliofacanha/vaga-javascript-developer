angular.module('directives', [])
	.directive('xButton', function() {
        var button = {};
        button.restrict = "E";
        button.scope = {
            name: '@',
            action : '&'
        }
        button.template = '<button class="btn btn-default col-md-12" ng-click="action()">{{name}}</button>';

        return button;
    });