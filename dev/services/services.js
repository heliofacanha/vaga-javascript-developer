angular.module('services', ['ngResource'])
    .factory('platforms', function($resource) {
        //Recupera lsita de plataformas
		return $resource('http://private-59658d-celulardireto2017.apiary-mock.com/plataformas ', null, {
			'list' : { 
				method: 'GET'
			}
		});
    })
    .factory('plans', function($resource) {
        //Recupera lista de Planos com base no sku da plataforma escolhida
		return $resource('http://private-59658d-celulardireto2017.apiary-mock.com/planos/:sku', null, {
			'list' : { 
                method: 'GET',
                params: {
                    sku: '@sku'
                },
			}
		});
	})
	.factory("dataSteps", function() {
        //Concentra todos os dados que o usuário for escolhendo
		var data = {
            platform: {},
            plan: {},
            user: {}
        };
            data.setPlatform = function(obj) {
                data.platform = obj
            };
            data.setPlan = function(obj) {
                data.plan = obj
            };
            data.setUser = function(obj) {
                data.user = {
                    nome: obj.name,
                    email: obj.email,
                    data: (typeof obj.date == 'object')? obj.date.toLocaleString():obj.date,
                    cpf: obj.cpf,
                }
            };
            data.getData = function() {
                return data;
            }
		return data;
    });